package String;

public class Replace {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String Name = "Kuntal";
		String Name2= Name.replace("a", "o");
		System.out.println(Name);
		System.out.println(Name2);
		
		// hence we can see that strings are immutable. i,e. we can't change any string after deployment
		// so if we want to change them we have to create a another variable .
	}

}
